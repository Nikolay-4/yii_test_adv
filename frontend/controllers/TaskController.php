<?php
/**
 * Created by PhpStorm.
 * User: linux
 * Date: 09.08.18
 * Time: 14:04
 */

namespace frontend\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Controller;
use common\models\User;
use common\models\Task;
use yii\web\HttpException;
use yii\web\Response;

class TaskController extends ActiveController
{
    public $modelClass = 'common\models\Task';

    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'except' => ['index', 'view']
            ],

            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'except' => ['index', 'view'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['crudTask']
                    ],
                ],
            ],
        ];
    }


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['options']);
        unset($actions['create']);
        return $actions;
    }

    public function actionCreate(){
        $request = \Yii::$app->request;

        $task = new Task();
        $task->description = $request->post('description');
        $task->tasklist_id = $request->post('tasklist_id');
        $task->user_id = \Yii::$app->user->id;

        if(!$task->validate()){
            return $task->errors;
        }
        $task->save();
        return $task;
    }
}
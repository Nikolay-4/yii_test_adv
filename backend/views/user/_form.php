<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-list-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        <?= $form->field($model, 'email')->textInput(['readonly' => true]) ?>
        <?= $form->field($model, 'tasklist_limit')->textInput(['readonly', 'type' => 'number']) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

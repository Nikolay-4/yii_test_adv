<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskList */

$this->title = 'Update User: ' . $model->username;
?>
<div class="task-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

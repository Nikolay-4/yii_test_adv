<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaskList */

$this->title = 'Create Task List';
$this->params['breadcrumbs'][] = ['label' => 'Task Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-list-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>

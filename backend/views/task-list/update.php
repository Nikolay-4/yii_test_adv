<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskList */

$this->title = 'Update Task List: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Task Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="task-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

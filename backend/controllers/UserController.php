<?php

namespace backend\controllers;

use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['adminPanel']
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['editTaskListLimit']
                    ]
                ]
            ]

//            [
//                'class' => 'common\filters\RbacFilter',
//                'action_permission' => [
//                    'index' => 'adminPanel',
//                    'view' => 'adminPanel',
//                    'create' => 'adminPanel',
//                    'update' => 'editTaskListLimit',
//                    'delete' => 'adminPanel',
//                ]
//            ],
        ];
    }


    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(Yii::$app->request->method == 'POST'){
            $new_limit = Yii::$app->request->post('User')['tasklist_limit'];
            if(!\Yii::$app->user->can('changeTaskListLimit', ['user' => $model, 'new_limit' => $new_limit])){
                $model->addError('tasklist_limit', 'too many task lists');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            $model->tasklist_limit = $new_limit;
            $model->save(false);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

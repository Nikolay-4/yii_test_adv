<?php

namespace backend\controllers;

use common\models\User;
use Yii;
use yii\helpers\Url;
use common\models\Task;
use backend\models\TaskSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class AuthController extends Controller
{

    public function actionLogin(){

        $this->layout = 'login';
        $model = new User();
        $model->setScenario('login');

        if(\Yii::$app->request->method == 'GET'){
            return $this->render('login', ['model' => $model]);
        }

        $model->attributes = \Yii::$app->request->post('User');
        if($model->validate()){
            Yii::$app->user->login(User::findOne(['username' => $model->username]));

            return $this->redirect('/');
        } else{
            return $this->render('login', ['model' => $model]);
        }

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['adminPanel']
                    ],
                ]
            ]
        ];
    }

    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->redirect(Url::toRoute('login'));
    }
}

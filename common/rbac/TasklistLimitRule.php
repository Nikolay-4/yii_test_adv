<?php
/**
 * Created by PhpStorm.
 * User: linux
 * Date: 24.08.18
 * Time: 11:36
 */

namespace common\rbac;

use yii\rbac\Rule;

class TasklistLimitRule extends Rule
{
    public $name = 'isLimit';

    public function execute($user, $item, $param){
        try{
            $user_model = $param['user'];
            $new_limit = (int)$param['new_limit'];
            $tasklists_count = count($user_model->taskLists);
            if(!isset($user_model) or !isset($new_limit) or $tasklists_count > $new_limit){
                return false;
            }
        } catch (\Exception $e){
            return false;
        }
        return true;
    }
}
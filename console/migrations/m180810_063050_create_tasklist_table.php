<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasklist`.
 */
class m180810_063050_create_tasklist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tasklist', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'user_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'fk-tasklist-user_id',
            'tasklist',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tasklist');
    }
}

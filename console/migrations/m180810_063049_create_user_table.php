<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180810_063049_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull()->unique(),
            'email' => $this->string(255),
            'tasklist_limit' =>$this->integer(),
            'password' => $this->string(255)->notNull(),
            'auth_key' => $this->string(),
            'access_token' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
